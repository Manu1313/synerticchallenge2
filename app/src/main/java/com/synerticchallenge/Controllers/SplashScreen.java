package com.synerticchallenge.Controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.synerticchallenge.Managers.DataManager;
import com.synerticchallenge.Models.Pins;
import com.synerticchallenge.Models.UserData;
import com.synerticchallenge.R;
import com.synerticchallenge.Utils.CallbackInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class SplashScreen extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        performRoutine();
    }

    public void performRoutine()
    {
        DataManager manager = new DataManager();
        final UserData user = new UserData();
        final Pins pins = new Pins();

        manager.performRoutine(this, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {
            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {
                ArrayList<String> titles = new ArrayList<>();
                ArrayList<String> urls = new ArrayList<>();
                ArrayList<String> descriptions = new ArrayList<>();
                ArrayList<String> dates = new ArrayList<>();

                for (int i = 0; i < object.size(); i += 1)
                {
                    try {
                        titles.add(object.get(i).get("title").toString());
                        urls.add(object.get(i).get("content_url").toString());
                        descriptions.add(object.get(i).get("description").toString());
                        dates.add(object.get(i).get("date").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                pins.setTitles(titles);
                pins.setUrls(urls);
                pins.setDates(dates);
                pins.setDescriptions(descriptions);
                pins.setObject(object);
                goHome();
            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

    public void goHome()
    {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
