package com.synerticchallenge.Models;

import org.json.JSONObject;

import java.util.ArrayList;

public class Pins {
    private static ArrayList<String> titles;
    private static ArrayList<String> urls;
    private static ArrayList<String> descriptions;
    private static ArrayList<String> dates;
    private static ArrayList<JSONObject> object;

    public Pins()
    {

    }

    public ArrayList<JSONObject> getObject() {
        return object;
    }

    public void setObject(ArrayList<JSONObject> object) {
        Pins.object = object;
    }

    public ArrayList<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(ArrayList<String> descriptions) {
        Pins.descriptions = descriptions;
    }

    public ArrayList<String> getDates() {
        return dates;
    }

    public void setDates(ArrayList<String> dates) {
        Pins.dates = dates;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

    public void setTitles(ArrayList<String> titles) {
        Pins.titles = titles;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        Pins.urls = urls;
    }
}
