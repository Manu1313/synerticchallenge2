package com.synerticchallenge.Models;

public class UserData {
    private static String wsToken;
    private static String refreshToken;
    private static String login;
    private static String password;

    public UserData()
    {
    }

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        UserData.wsToken = wsToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        UserData.refreshToken = refreshToken;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        UserData.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        UserData.password = password;
    }
}
