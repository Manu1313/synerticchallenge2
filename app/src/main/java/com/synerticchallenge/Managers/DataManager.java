package com.synerticchallenge.Managers;

import android.content.Context;
import android.util.Log;
import com.synerticchallenge.Models.UserData;
import com.synerticchallenge.Utils.CallbackInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class DataManager {

    private ServiceProvider service = new ServiceProvider();
    private UserData user = new UserData();
    public void performRoutine(final Context context, final CallbackInterface callbackInterface)
    {
        authentification(context, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {
                user.setWsToken(wsToken);
                getPins(context, new CallbackInterface() {
                    @Override
                    public void callbackUserById(JSONObject userInfos) {

                    }

                    @Override
                    public void callbackAuth(String wsToken) {

                    }

                    @Override
                    public void callbackPings(ArrayList<JSONObject> object) {
                        callbackInterface.callbackPings(object);
                    }

                    @Override
                    public void requestPerformed(ResponseBody response, Exception e) {

                    }

                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Log.e("lol", response.body().string());
                    }
                });
                //callbackInterface.callbackAuth(wsToken);
            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {

            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

    public void authentification(Context context, final CallbackInterface callbackInterface)
    {
        service.authentification(context, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {
                callbackInterface.callbackAuth(wsToken);
            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {

            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {

            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                requestPerformed(response.body(), null);
            }
        });
    }

    public void getPins(Context context, final CallbackInterface callbackInterface)
    {
        service.getPins(context, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {
            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {
                callbackInterface.callbackPings(object);
            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {

            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                requestPerformed(response.body(), null);
            }
        });
    }
}
