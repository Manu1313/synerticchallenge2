package com.synerticchallenge.Managers;

import com.google.gson.Gson;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import com.google.gson.GsonBuilder;
import com.synerticchallenge.Models.UserData;
import com.synerticchallenge.Utils.CallbackInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import okhttp3.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

enum Endpoints {
    AUTHENTIFICATION("/authentification"),
    GET_PINS("/getPins"),
    GET_USERS("/getUsers"),
    GET_PIN_BY_ID("/getPinById"),
    GET_USER_BY_ID("/getUserById");

    private String name = "";

    //Constructeur
    Endpoints(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}

enum HTTPMethod {
    POST("post"),
    GET("get");

    private String name = "";

    //Constructeur
    HTTPMethod(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}

public class ServiceProvider
{

    private static String baseUrl = "http://challenge.synertic.net/services/request";

    public OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build();


    private Gson gson = new GsonBuilder().create();

    public static Request getRequest(HTTPMethod httpMethod, Endpoints endpoint, ArrayList<HashMap<String, String>> queryParameters, HashMap<String, Object> bodyParams)
    {
        Request request = null;
        String url = baseUrl + endpoint.toString();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

        if (urlBuilder != null && queryParameters != null) {

            for (HashMap<String, String> aParameter : queryParameters) {
                for (HashMap.Entry<String, String> entry : aParameter.entrySet()) {
                    urlBuilder.addQueryParameter(entry.getKey(), entry.getValue());
                }
            }

            url = urlBuilder.build().toString();
        }

        if (httpMethod == HTTPMethod.GET) {
            request = new Request.Builder()
                    .url(url)
                    .build();
        }
        else if (httpMethod == HTTPMethod.POST) {
            if (bodyParams != null) {
                String JsonString = new Gson().toJson(new Gson().toJsonTree(bodyParams).getAsJsonObject());
                RequestBody body =
                        RequestBody.create(MediaType.parse("text/plain"), JsonString);
                request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();
            }
        }
        return request;
    }

    // This method will perform request and return via callback any reponse and Exception
    // It will also Log all informations about current request
    public void performRequest(Request request, Context context, final CallbackInterface callbackInterface)
    {
        if (request == null || context == null) { return;}

        this.client.newCall(request).enqueue(new CallbackInterface() {

            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {

            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {

            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {

            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if (response.code() == 200) {
                    Log.i("Info", "Request did succeed: 200");
                    callbackInterface.requestPerformed(response.body(), null);
                }
                else
                {
                    Log.i("Info", "Request did failed: 400");
                    callbackInterface.requestPerformed(null, null);
                }
            }

            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.i("Info", "Request did failed: 400");
                callbackInterface.requestPerformed(null, e);
            }
        });
    }

    public void getUserById(Context context, Integer userId, final CallbackInterface callback)
    {
        final UserData user = new UserData();
        HashMap<String, Object> hashmap = new HashMap<>();
        hashmap.put("wsToken", user.getWsToken());
        hashmap.put("userId", userId);

        // Request with Endpoint /users/anonymous and with current userId in Url parameters
        Request request = ServiceProvider.getRequest(HTTPMethod.POST, Endpoints.GET_USER_BY_ID, null, hashmap);
        performRequest(request, context, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {
            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {
            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {
                try {
                    JSONObject obj = new JSONObject(response.string());
                    callback.callbackUserById((JSONObject) obj.get("result"));
                } catch (IOException | JSONException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

    public void getPins(Context context, final CallbackInterface callback)
    {
        final UserData user = new UserData();
        HashMap<String, Object> hashmap = new HashMap<>();
        hashmap.put("wsToken", user.getWsToken());
        hashmap.put("index", 1);

        // Request with Endpoint /users/anonymous and with current userId in Url parameters
        Request request = ServiceProvider.getRequest(HTTPMethod.POST, Endpoints.GET_PINS, null, hashmap);
        performRequest(request, context, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {
            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {
            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {
                String result = null;
                ArrayList<JSONObject> array = new ArrayList<>();
                try {
                    result = response.string();
                    JSONObject obj = new JSONObject(result);
                    JSONArray obj2 = (JSONArray) obj.get("result");
                    for (int i = 0; i < obj2.length(); i++) {
                        JSONObject jsonobject = obj2.getJSONObject(i);
                        array.add(jsonobject);
                    }
                    callback.callbackPings(array);
                } catch (IOException | JSONException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }

    public void authentification(Context context, final CallbackInterface callback)
    {
        HashMap<String, Object> hashmap = new HashMap<>();
        final UserData user = new UserData();

        hashmap.put("login", "user_test_challenger");
        hashmap.put("password", "0ed774fb6a9758fb54114d2132a47574");
        user.setLogin("user_test_challenger");
        user.setPassword("0ed774fb6a9758fb54114d2132a47574");

        // Request with Endpoint /users/anonymous and with current userId in Url parameters
        Request request = ServiceProvider.getRequest(HTTPMethod.POST, Endpoints.AUTHENTIFICATION, null, hashmap);
        performRequest(request, context, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

            }

            @Override
            public void callbackAuth(String wsToken) {

            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {

            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {
                String result = null;
                try {
                    result = response.string();
                    JSONObject obj = new JSONObject(result);
                    JSONObject obj2 = (JSONObject) obj.get("result");
                    user.setRefreshToken((obj2.get("refreshToken").toString()));
                    callback.callbackAuth(obj2.get("wsToken").toString());
                } catch (IOException | JSONException e1) {
                    e1.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }
}