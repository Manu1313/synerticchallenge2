package com.synerticchallenge.Utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import java.util.ArrayList;

public class Utils {

    private static ArrayList<Object> listOfFragment = new ArrayList<>();

    public void addFragment(FragmentManager manager, Fragment fragment, Integer frameId)
    {
        if (fragment == null)
            return;
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
        listOfFragment.add(fragment);
    }


    public void removeFragment(Fragment fragment)
    {
        if (fragment == null)
            return;
        FragmentTransaction transaction = fragment.getFragmentManager().beginTransaction();
        transaction.remove(fragment);
        transaction.commitAllowingStateLoss();
        listOfFragment.remove(fragment);
    }
}
