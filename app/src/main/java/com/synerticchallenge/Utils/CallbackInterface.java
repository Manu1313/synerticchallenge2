package com.synerticchallenge.Utils;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Callback;
import okhttp3.ResponseBody;

public interface CallbackInterface extends Callback {

    public void callbackUserById(JSONObject userInfos);
    public void callbackAuth(String wsToken);
    public void callbackPings(ArrayList<JSONObject> object);
    public void requestPerformed(ResponseBody response, Exception e);
}