package com.synerticchallenge.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.synerticchallenge.Controllers.MainActivity;
import com.synerticchallenge.Models.Pins;
import com.synerticchallenge.Models.UserData;
import com.synerticchallenge.R;

import org.json.JSONException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.internal.Util;

import static java.security.AccessController.getContext;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private Context context;
    Pins pins = new Pins();
    Utils utils = new Utils();

    public RecyclerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.elements, parent, false);
        ViewHolder recyclerHolder = new ViewHolder(layoutView);
        return recyclerHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        URL url = null;
        try {
            url = new URL(pins.getUrls().get(position));
            Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            holder.imageView.requestLayout();
            holder.imageView.setImageBitmap(bmp);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        holder.textView.setText(pins.getTitles().get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof MainActivity) {
                    Bundle bundle = new Bundle();
                    bundle.putString("title", pins.getTitles().get(position));
                    bundle.putString("url", pins.getUrls().get(position));
                    bundle.putString("description", pins.getDescriptions().get(position));
                    bundle.putString("date", pins.getDates().get(position));
                    try {
                        bundle.putInt("user_id",  pins.getObject().get(position).getInt("user_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    FragmentOpenPin fragmentOpenPin = new FragmentOpenPin();
                    fragmentOpenPin.setArguments(bundle);
                    utils.addFragment(((MainActivity) context).getFragmentManager(), fragmentOpenPin, R.id.main);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return pins.getTitles().size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            textView = (TextView) itemView.findViewById(R.id.title);
        }
    }
}