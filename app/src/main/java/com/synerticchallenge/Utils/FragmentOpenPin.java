package com.synerticchallenge.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;

import com.synerticchallenge.Controllers.MainActivity;
import com.synerticchallenge.Managers.ServiceProvider;
import com.synerticchallenge.Models.Pins;
import com.synerticchallenge.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class FragmentOpenPin extends Fragment {

    Utils utils = new Utils();
    Pins pins = new Pins();
    ServiceProvider service = new ServiceProvider();
    Activity currentActivity = null;
    View currentView = null;
    String url = null;
    String title = null;
    String description = null;
    String date = null;
    Integer user_id = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        url = getArguments().getString("url");
        title = getArguments().getString("title");
        description = getArguments().getString("description");
        date = getArguments().getString("date");
        user_id = getArguments().getInt("user_id");

        return inflater.inflate(R.layout.fragment_pin,
                container, false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        this.currentActivity.onBackPressed();
        return true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.currentView = view;
        this.currentActivity = this.getActivity();

        ImageView imageView = view.findViewById(R.id.image_pin);
        TextView titleText = view.findViewById(R.id.title_pin);
        TextView descriptionText = view.findViewById(R.id.description_pin);
        TextView dateText = view.findViewById(R.id.date_pin);
        final TextView userText = view.findViewById(R.id.user_pin);
        final ImageView imageView2 = view.findViewById(R.id.image_user);

        service.getUserById(currentActivity, user_id, new CallbackInterface() {
            @Override
            public void callbackUserById(JSONObject userInfos) {

                try {
                    userText.setText(userInfos.get("firstName") + "--" + userInfos.get("lastName"));
                    try {
                        URL picture = new URL(userInfos.getString("picture"));
                        final Bitmap bmp = BitmapFactory.decodeStream(picture.openConnection().getInputStream());
                        currentActivity.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                imageView2.setImageBitmap(bmp);
                            }
                        });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void callbackAuth(String wsToken) {

            }

            @Override
            public void callbackPings(ArrayList<JSONObject> object) {
            }

            @Override
            public void requestPerformed(ResponseBody response, Exception e) {

            }

            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });

        Bitmap bmp = null;
        try {
            bmp = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
            imageView.requestLayout();
            imageView.setImageBitmap(bmp);
        } catch (IOException e) {
            e.printStackTrace();
        }
        titleText.setText(title);
        descriptionText.setText(description);
        dateText.setText(date);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Activity cA = this.currentActivity;
        if (cA instanceof MainActivity) {
            utils.removeFragment(this);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }
}